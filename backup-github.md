#Backup your github account

install the NPM package "repos"

```console
npm install -g repos
```

get a list of all your repos

```console
npx repos ams0 ams0-repos.json
```

get just the clone_url's

```console
cat ams0-repos.json|  jq -r '.[].clone_url'| grep ams0 > repos.list
```

clone all your repos!

```console
for repo in `cat repos.list`; do git clone $repo; done
```

Clean up your Github account

```console
for repo in `cat repos.list| cut -c 20-| rev | cut -c5- | rev`; do gh api -X DELETE /repos/$repo >/dev/null 2>&1 ; done
```
